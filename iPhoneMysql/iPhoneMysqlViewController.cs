using MonoTouch.UIKit;
using System.Drawing;
using System;
using MonoTouch.Foundation;
using iPhoneMysql.WebService;
using System.Data;

//using iPhoneMysql.ServiceWeb;

namespace iPhoneMysql
{
	public partial class iPhoneMysqlViewController : UIViewController
	{
		public iPhoneMysqlViewController () : base ("iPhoneMysqlViewController", null)
		{
		}
		
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			iMysql service = new iMysql ();
			btnInsert.TouchUpInside += delegate {
				txtId.Text = service.InsertMovie (txtTitle.Text, txtProducer.Text, txtReleaseDate.Text,
					Convert.ToInt32 (txtCopyCost.Text), Convert.ToInt32 (txtRentalPrice.Text),
					Convert.ToInt32 (txtMovieCategory.Text), Convert.ToInt32 (txtRentalType.Text), txtRating.Text).ToString ();
			};
			
			btnGetMovie.TouchUpInside += delegate {
				DataSet setData = new DataSet ();
				DataRow row;
				setData = service.GetMovie (txtId.Text);
				
				row = setData.Tables ["ConceptualMovie"].Rows [0];
				string test = row[0].ToString();
				string title = row [1].ToString ();
				string Producer = row [2].ToString ();
				string ReleaseDate = row [3].ToString ();
				string CopyCost = row [4].ToString ();
				string RentalPrice = row [5].ToString ();
				string MovieCategory = row [6].ToString ();
				string RentalType = row [7].ToString ();
				string Rating = row [8].ToString ();
				
				string result1 ="Title: "+test +"\n Producer: "+ title + "\n Release Date: " + Producer
					+ "\n Copy Cost: " + ReleaseDate + "\n Rental Price: " + CopyCost + "\n Movie Cat: "
						+ RentalPrice + "\n Rental Type: " + MovieCategory + "\n RetalType: " + RentalType + "\n Rating: "
						+ Rating;
				
				UIAlertView alert = new UIAlertView () { Title = "Result ", Message = result1};
				alert.AddButton ("OK");
				alert.Show ();
				
			};
			//any additional setup after loading the view, typically from a nib.
		}
		
		public override void ViewDidUnload ()
		{
			base.ViewDidUnload ();
			
			// Release any retained subviews of the main view.
			// e.g. myOutlet = null;
		}
		
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			// Return true for supported orientations
			return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
		}
	}
}
