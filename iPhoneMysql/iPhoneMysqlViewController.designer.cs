// WARNING
//
// This file has been generated automatically by MonoDevelop to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace iPhoneMysql
{
	[Register ("iPhoneMysqlViewController")]
	partial class iPhoneMysqlViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnGetMovie { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnReset { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnInsert { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtTitle { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtProducer { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtReleaseDate { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtCopyCost { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtRentalPrice { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtMovieCategory { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtRentalType { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtRating { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtId { get; set; }
	}
}
